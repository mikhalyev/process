# Process - primitive framework for process execution.
             
## Description functionality
The current framework allows you to sequentially run some abstract process configured through configuration. 
Step by step execute user defined action for each step. 
You can customize
1. input data provider
2. output data provider
3. storage of process

## Getting started
For usage current framework you have some like scenarios of connection to yours code  
1. **Manually** 
   
    Download code and manually add framework classes to yours class autoloader.
   

2. **Composer**
   
   Need add some section in composer.json  
   first define custom repo
    ``` 
    "repositories": [
        {
            "type": "git",
            "url": "git@gitlab.com:mikhalyev/process.git"    
        }
   ]
   ```
      
    second add to require section
    ```
   "require": {
        ...
        "lp/executer" : "*",
        ...
   }
    ```

## Simple example usage
The framework is very easy to use.
NOTE: In this we consider that framework installed via composer.  

In current example use: file for input data (yaml format), file for output data (yaml format) and elementary storage in php memory.

Process file configuration in yaml format: 
```yaml
input: \Ip\Process\Io\Reader\YamlFileReader
output: \Ip\Process\Io\Writer\YamlFileWriter
storage: \Ip\Process\Storage\ProcessMemoryStorage
steps:        
  - class: \Demo\Example\Step1
  - class: \Demo\Example\Step2
  - class: \Demo\Example\StepFinal    
```
         
php
```injectablephp
    $configFile = '/path/to/proccess/config.yaml';
    $inputSourceFile = ['filePath' => '/path/to/input.yaml'];    
    $outputSourceFile = ['filePath' => '/path/to/output.yaml'];
    
    $process = \Ip\Process\ProcessFactory::create($configFile);
    $execService = new \Ip\Process\Service();
    $processContext = $execService->execute($process, $inputSourceFile, $outputSourceFile);
```
    
## Customisation  
                
### Customisation input data provider  
You can create custom data provider that will be reading data from your source (file, DataBase,  APi and etc)  
For this you must implements interface Ip\Process\Contracts\InputProviderInterface

Main method is getData()
Method for some configuration is setSource().  

                                     
### Customisation output data provider
You can create custom data provider that will be send data to your source (file, DataBase, APi and etc)
For this you must implements interface Ip\Process\Contracts\OutputProviderInterface
                        
Main method is getData()
Method for some configuration is setSource().

### Customisation storage
You can create custom data storage that will be store execution step data into (file, DataBase, APi and etc)
For this you must implements interface Ip\Process\Contracts\StorageInterface