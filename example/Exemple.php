<?php
require dirname(__DIR__).'/vendor/autoload.php';

try {
    $configFile = dirname(__DIR__) . '/example/process.yaml';
    $inputSourceFile = ['filePath' => dirname(__DIR__) . '/example/input.yaml'];
    $outputSourceFile = ['filePath' => dirname(__DIR__) . '/example/output.yaml'];

    $process = \Ip\Process\ProcessFactory::create($configFile);
    $exe = new \Ip\Process\Service();
    $processContext = $exe->execute($process, $inputSourceFile, $outputSourceFile);
} catch (Exception $e) {
    die ($e->getMessage());
}
