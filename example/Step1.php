<?php

namespace Demo\Example;

use Ip\Process\Contracts\StepInterface;
use Ip\Process\Contracts\StorageInterface;

class Step1 implements StepInterface
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    public function setStorage(StorageInterface $storage): void
    {
        $this->storage = $storage;
    }

    static public function getIdentity(): string
    {
        return self::class;
    }

    public function execute(string $context, array $input): array
    {
        $a = $input['a'] ?? null;
        if (null === $a) {
            throw new \Exception('Undefined binomial coefficient "a"');
        }

        if (!is_numeric($a)) {
            throw new \Exception('Invalid binomial coefficient "a"');
        }

        $x = $input['x'] ?? null;
        if (null === $x) {
            throw new \Exception('Undefined variable "x"');
        }
        if (!is_numeric($x)) {
            throw new \Exception('Invalid variable "x"');
        }

        return [
            'term' => $a * pow($x, 2)
        ];
    }
}
