<?php

namespace Demo\Example;

use Ip\Process\Contracts\StepInterface;
use Ip\Process\Contracts\StorageInterface;

class Step2 implements StepInterface
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    static public function getIdentity(): string
    {
        return self::class;
    }

    public function setStorage(StorageInterface $storage): void
    {
        $this->storage = $storage;
    }

    public function execute(string $context, array $input): array
    {
        $b = $input['b'] ?? null;
        if (null === $b) {
            throw new \Exception('Undefined binomial coefficient "b"');
        }

        $x = $input['x'];
        if (!is_numeric($b)) {
            throw new \Exception('Invalid binomial coefficient "b"');
        }

        return [
            'term' => $b * $x
        ];
    }
}
