<?php

namespace Demo\Example;

use Ip\Process\Contracts\StepInterface;
use Ip\Process\Contracts\StorageInterface;

class StepFinal implements StepInterface
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    static public function getIdentity(): string
    {
        return self::class;
    }

    public function setStorage(StorageInterface $storage): void
    {
        $this->storage = $storage;
    }

    public function execute(string $context, array $input): array
    {
        $step1Result = $this->storage->read($context, Step1::getIdentity());
        $step2Result = $this->storage->read($context, Step2::getIdentity());

        return [
            'result' => $step1Result['term'] + $step2Result['term']
        ];
    }
}
