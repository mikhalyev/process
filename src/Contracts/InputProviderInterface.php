<?php

namespace Ip\Process\Contracts;

interface InputProviderInterface
{
    public function setSource($config): void;

    public function getData(): array;
}
