<?php

namespace Ip\Process\Contracts;

interface OutputProviderInterface
{
    /**
     *
     * @param mixed $source
     * @return mixed
     */
    public function setSource($source): void;

    public function write(string $processId, array $data): void;
}
