<?php

namespace Ip\Process\Contracts;

interface ProcessInterface
{
    /**
     * Get collection of StepInterface objects of process
     *
     * @return StepInterface[]
     */
    public function getSteps(): array;

    /**
     * Get input provider object for retrieve input data
     *
     * @return InputProviderInterface
     */
    public function getInputProvider(): InputProviderInterface;

    /**
     * Get output provider object for send output data
     *
     * @return OutputProviderInterface
     */
    public function getOutputProvider(): OutputProviderInterface;

    /**
     * Get storage object for save result of each executed step
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface;
}
