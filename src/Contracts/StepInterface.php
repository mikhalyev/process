<?php

namespace Ip\Process\Contracts;

interface StepInterface
{
    /**
     * Get string identifier of step process
     * @return string
     */
    static public function getIdentity(): string;

    /**
     * Set process storage
     * Used to get the result of previous steps taken
     *
     * @param StorageInterface $storage
     */
    public function setStorage(StorageInterface $storage): void;

    /**
     * Calculate step
     *
     * @param string $context
     * @param array $input
     * @return array
     */
    public function execute(string $context, array $input): array;
}
