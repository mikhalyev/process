<?php

namespace Ip\Process\Contracts;

interface StorageInterface
{
    /**
     * Read data from storage
     *
     * @param string $context
     * @param string $step
     * @return array|null
     */
    public function read(string $context, string $step): ?array;

    /**
     * Write data to storage
     *
     * @param string $context
     * @param string $step
     * @param array $data
     * @return mixed
     */
    public function write(string $context, string $step, array $data);
}
