<?php
namespace  Ip\Process\Io\Reader;

use Ip\Process\Contracts\InputProviderInterface;

class CsvFileReader implements InputProviderInterface
{
    protected $filePath;

    public function setSource($config): void
    {
        $this->filePath = $config['filePath'] ?? '';
    }

    public function getData(): array
    {
        $rawData = file_get_contents($this->filePath);
        return str_getcsv($rawData);
    }
}