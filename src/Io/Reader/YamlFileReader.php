<?php
namespace  Ip\Process\Io\Reader;

use Ip\Process\Contracts\InputProviderInterface;
use Symfony\Component\Yaml\Yaml;

class YamlFileReader implements InputProviderInterface
{
    protected $filePath;

    public function setSource($config): void
    {
        $this->filePath = $config['filePath'] ?? '';
    }

    public function getData(): array
    {
        return Yaml::parseFile($this->filePath);
    }
}