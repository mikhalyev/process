<?php
namespace  Ip\Process\Io\Writer;

use Ip\Process\Contracts\OutputProviderInterface;
use Symfony\Component\Yaml\Dumper as YamlDumper;

class YamlFileWriter implements OutputProviderInterface
{
    protected $filePath;

    public function setSource($source): void
    {
        $this->filePath = $source['filePath'] ?? '';
    }

    public function write(string $identity, array $data): void
    {
        $dumper = new YamlDumper();
        $rawYaml = $dumper->dump([
            $identity => $data
        ]);
        file_put_contents($this->filePath, $rawYaml, FILE_APPEND);
    }
}