<?php

namespace Ip\Process;

use Ip\Process\Contracts\InputProviderInterface;
use Ip\Process\Contracts\OutputProviderInterface;
use Ip\Process\Contracts\ProcessInterface;
use Ip\Process\Contracts\StepInterface;
use Ip\Process\Contracts\StorageInterface;

/**
 * Class Process describe a process of solve some task step by step.
 *
 * @package Ip\Process
 */
class Process implements ProcessInterface
{
    /**
     * @var StepInterface[]
     */
    protected $steps;

    /**
     * @var InputProviderInterface
     */
    protected $inputProvider;

    /**
     * @var OutputProviderInterface
     */
    protected $outputprovider;

    /**
     * @var StepInterface
     */
    protected $storage;

    public function __construct(
        InputProviderInterface $inputProvider,
        OutputProviderInterface $outputProvider,
        StorageInterface $storage,
        array $steps
    )
    {
        $this->inputProvider = $inputProvider;
        $this->outputprovider = $outputProvider;
        $this->storage = $storage;
        $this->steps = $steps;
    }

    /**
     * @return StepInterface[]
     */
    public function getSteps(): array
    {
        return $this->steps;
    }

    /**
     * @return InputProviderInterface
     */
    public function getInputProvider(): InputProviderInterface
    {
        return $this->inputProvider;
    }

    /**
     * @return OutputProviderInterface
     */
    public function getOutputProvider(): OutputProviderInterface
    {
        return $this->outputprovider;
    }

    /**
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }
}