<?php

namespace Ip\Process;

use Ip\Process\Contracts\ProcessInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

class ProcessFactory
{
    /**
     * @param string $pathToConfigFile
     * @param ContainerInterface $container
     * @return ProcessInterface
     * @throws \Exception
     */
    static public function create(string $pathToConfigFile, ?ContainerInterface $container = null): ProcessInterface
    {
        $config = Yaml::parseFile($pathToConfigFile);

        $storage = null;
        $storageClass = $config['storage'] ?? null;
        if (null === $storageClass) {
            throw new \Exception('Configuration has not valid format. Section "storage" must be exist');
        }
        if ($container && $container->has($storageClass)) {
            $storage = $container->get($storageClass);
        } else {
            $storage = new $storageClass();
        }

        $inputProvider = null;
        $inputProviderClass = $config['input'] ?? null;
        if (null === $inputProviderClass) {
            throw new \Exception('Configuration has not valid format. Section "input" must be exist');
        }
        if ($container && $container->has($inputProviderClass)) {
            $inputProvider = $container->get($inputProviderClass);
        } else {
            $inputProvider = new $inputProviderClass();
        }

        $outputProvider = null;
        $outputProviderClass = $config['output'] ?? null;
        if (null === $outputProviderClass) {
            throw new \Exception('Configuration has not valid format. Section "output" must be exist');
        }
        if ($container && $container->has($outputProviderClass)) {
            $outputProvider = $container->get($outputProviderClass);
        } else {
            $outputProvider = new $outputProviderClass();
        }

        $stepsData = $config['steps'] ?? null;
        if (null === $stepsData || !is_array($stepsData)) {
            throw new \Exception('Configuration has not valid format. Section "steps" must be exist and should be is collection');
        }
        $steps = [];
        foreach ($stepsData as $stepCfg) {
            $stepClass = $stepCfg['class'] ?? null;
            if ($container && $container->has($stepClass)) {
                $steps[] = $container->get($stepClass);
            } else {
                $steps[] = new $stepClass();
            }
        }

        return new Process(
            $inputProvider,
            $outputProvider,
            $storage,
            $steps
        );
    }
}