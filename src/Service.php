<?php

namespace Ip\Process;

use Ip\Process\Contracts\ProcessInterface;
use Psr\Log\LoggerInterface;

class Service
{
    /** @var LoggerInterface */
    protected $logger;

    /**
     * @throws \Exception
     */
    public function __construct(?LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @param ProcessInterface $process
     * @param $inputSource
     * @param $outputSource
     * @return string
     * @throws \Exception
     */
    public function execute(ProcessInterface $process, $inputSource, $outputSource): string
    {
        $context = uniqid();
        try {
            $process->getInputProvider()->setSource($inputSource);
            $process->getOutputProvider()->setSource($outputSource);

            $storage = $process->getStorage();
            $initialProcessData = $process->getInputProvider()->getData();
            $stepResult = null;
            foreach ($process->getSteps() as $step) {
                $step->setStorage($storage);
                $stepResult = $step->execute($context, $initialProcessData);
                $storage->write($context, $step::getIdentity(), $stepResult);
            }

            $process->getOutputProvider()->write($context, $stepResult);
        } catch (\Exception $e) {
            if (null !== $this->logger) {
                $this->logger->error($e->getMessage(), [
                    'step' => $step::getIdentity(),
                    'context' => $context,
                    'trace' => $e->getTrace()
                ]);

                throw $e;
            }
        }

        return $context;
    }
}