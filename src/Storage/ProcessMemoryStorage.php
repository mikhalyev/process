<?php

namespace Ip\Process\Storage;

use Ip\Process\Contracts\StorageInterface;

class ProcessMemoryStorage implements StorageInterface
{
    static protected $memoryStorage = [];

    public function read(string $context, string $step): ?array
    {
        return self::$memoryStorage[$context][$step] ?? null;
    }

    public function write(string $context, string $step, array $data)
    {
        self::$memoryStorage[$context][$step] = $data;
    }
}